class AddIndexToAnswerChoice2 < ActiveRecord::Migration
  def change

    add_index(:answer_choices, :response_id)
  end
end
