class CreateQuestion < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.text :body_text

      t.timestamps
    end

    add_index(:questions, :poll_id)


  end
end
