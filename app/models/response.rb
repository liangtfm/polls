class Response < ActiveRecord::Base

  belongs_to(
  :answer_choice,
  class_name: "AnswerChoice",
  foreign_key: :answer_choice_id,
  primary_key: :id)

  belongs_to(
  :respondent,
  class_name: "User",
  foreign_key: :user_id,
  primary_key: :id)


  def respondent_isnt_author
    # join User to polls to questions to answer_choices
    # 1 - user join polls to find authored_polls
    # 2 - polls join questions to find answer_choices
    # 3 - questions join answer_choices to find answer_choices.id

    # 4 - see if answer_choices.id is included within answer_choices
    # to questions to authored polls
    # self.answer_choice_id included in query?

    id = self.respondent.id

    Poll.find(Question.find(AnswerChoice.find(self.answer_choice).question_id).poll_id).author_id != id

  end

  # def find_authored_polls
  #   id = self.respondent.id
  #   ac_id = self.answer_choice_id
  #
  #   query = <<-SQL
  #
  #
  #   SELECT r.*
  #   FROM responses AS r
  #   JOIN users AS U
  #   ON u.id = r.user_id
  #     JOIN polls AS p
  #     ON p.author_id = u.id
  #       JOIN questions AS q
  #       ON q.poll_id = p.id
  #         JOIN answer_choices AS ac
  #         ON ac.question_id = q.id
  #   WHERE p.author_id = ? -- AND ac.id = ?
  #   SQL
  #
  #   self.class.find_by_sql([query, id, ac_id])
  # end

  def respondent_has_not_already_answered_question

    (existing_responses.count == 1) &&
      (existing_responses[0] && (self.id == existing_responses[0].id))

  end

  def existing_responses
    query = <<-SQL
    SELECT r.*
    FROM responses AS r
    JOIN answer_choices AS ac
    ON r.answer_choice_id = ac.id
    WHERE r.user_id = ? AND ac.question_id =
      (SELECT ac.question_id
       FROM answer_choices AS ac
       WHERE ac.response_id = ?)

    SQL

    self.class.find_by_sql([query, self.respondent, self.id])
  end
end