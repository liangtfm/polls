class Question < ActiveRecord::Base

  attr_accessible :body_text

  validates :body_text, presence: true

  belongs_to(
  :poll,
  class_name: "Poll",
  foreign_key: :poll_id,
  primary_key: :id
  )

  has_many(
  :answer_choices,
  class_name: "AnswerChoice",
  foreign_key: :question_id,
  primary_key: :id
  )

  def results
    results_hash = {}

    query = <<-SQL
      SELECT ac.*, COUNT(ac.response_id) AS ans_count -- this becomes a method
      FROM questions q
      JOIN answer_choices ac
      ON q.id = ac.question_id
      GROUP BY ac.id
    SQL

    results = AnswerChoice.find_by_sql([query])

    results.each do |result|
      results_hash[result.choice_text] = result.ans_count
    end

    results_hash
  end

end

# User.joins("INNER JOIN polls ON polls.author_id = users.id")
# rails generate migration AddIndexToPoll